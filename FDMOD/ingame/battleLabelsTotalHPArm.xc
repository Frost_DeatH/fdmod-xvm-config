﻿/**
 * Battle inteface text fields.
 * ????????? ???? ??????? ??????????.
 */
{
	// ?????? ?? ?????? Arm
  "def": {
	// ??????? ???????? 
	"totalHP_bg": { 
      "enabled": ${"../fdmod_ui_TeamHPbar.xc":"."},
      "x": 0,
      "y": 31,
	  "width": 1000,
      "height": 49,
      "alpha": 20,
      "screenHAlign": "center",
	  "src": "xvm://res/icons/totalHP/Arm/bg.png"
    },
	// ??? ?????? ?? ?????????
	"totalHP_Bg_ally": { 
	  "enabled": ${"../fdmod_ui_TeamHPbar.xc":"."},
      "x": -118,
      "y": 37,
	  "width": 161,
      "height": 17,
	  "screenHAlign": "center",
      "src": "xvm://res/icons/totalHP/Arm/hp-bg.png"
    },
	// ??? ?????? ?? ???????????
	"totalHP_Bg_enemy": { 
	  "$ref": { "path":"def.totalHP_Bg_ally" },
      "x": 118
    },
	// ?????? ?? ?????????
	"totalHP_line_ally": { 
	  "enabled": ${"../fdmod_ui_TeamHPbar.xc":"."},
      "updateEvent": "PY(ON_UPDATE_HP)",
	  "x": "{{py:math.sum({{py:math.div({{py:xvm.total_hp.ally(158)}},-2)}}, -39)}}",
      "y": 38,
	  "width": "{{py:xvm.total_hp.ally(160)}}",
      "height": 15,
	  "screenHAlign": "center",
	  "src": "xvm://res/icons/totalHP/Arm/hp-ally.png" 
	},
	// ?????? ?? ???????????
	"totalHP_line_enemy": { 
	  "$ref": { "path":"def.totalHP_line_ally" },
	  "x": "{{py:math.sum({{py:math.div({{py:xvm.total_hp.enemy(158)}},2)}}, 39)}}",
	  "width": "{{py:xvm.total_hp.enemy(160)}}",
	  "src": "xvm://res/icons/totalHP/Arm/hp-enemy.png"
	},
	// ????? ?????????
	"totalHP_frags_ally": { 
	  "enabled": false,
      "updateEvent": "PY(ON_UPDATE_HP)",
      "x": 20,
      "y": 0,
	  "width": 30,
      "height": 30,
      "screenHAlign": "center",
	  "shadow": { "distance": 1, "angle": 90, "alpha": 80, "blur": 5, "strength": 1.5 },
      "textFormat": { "font": "$TitleFont", "size": 20, "align": "center" },
	  "format": "{{py:sp.allyFrags}}"
	},
	// ????? ???????????
	"totalHP_frags_enemy": { 
	  "$ref": { "path":"def.totalHP_frags_ally" },
      "x": 20,
      "format": "{{py:sp.enemyFrags}}"
	},
	// ??????????? ??????
	"totalHP_sing": { 
	  "enabled": false,
      "updateEvent": "PY(ON_UPDATE_HP)",
      "x": 0,
      "y": 40,
	  "width": 30,
      "height": 32,
	  "screenHAlign": "center",
	  "shadow": { "distance": 1, "angle": 90, "alpha": 80, "blur": 5, "strength": 1.5 },
      "textFormat": { "font": "$TitleFont", "size": 20, "align": "center" },
      "format": "<font size='20' color='{{py:math.sub({{py:xvm.total_hp.ally}}, {{py:xvm.total_hp.enemy}})>0?#5ACB00|#F30900}}'>|</font>"
    },
	// ??????? ?? ??
    "totalHP_advant": { 
	  "enabled": ${"../fdmod_ui_TeamHPbar.xc":"."},
      "updateEvent": "PY(ON_UPDATE_HP)",
      "x": 0,
      "y": 35,
	  "width": 60,
	  "height": 30,
	  "screenHAlign": "center",
	  "shadow": { "distance": 1, "angle": 90, "alpha": 80, "blur": 5, "strength": 1.5 },
      "textFormat": { "font": "$TitleFont", "size": 20, "align": "center" },
      "format": "<font size='17' color='{{py:math.sub({{py:xvm.total_hp.ally}}, {{py:xvm.total_hp.enemy}})>0?#5ACB00|#F30900}}'>{{py:math.sub({{py:xvm.total_hp.ally}}, {{py:xvm.total_hp.enemy}})}}</font>"
    },
	// ?? ?????????
	"totalHP_ally": {  
	  "enabled": ${"../fdmod_ui_TeamHPbar.xc":"."},
      "updateEvent": "PY(ON_UPDATE_HP)",
      "x": -232,
	  "y": 31,
	  "width": 70,
	  "height": 30,
      "screenHAlign": "center",
	  "shadow": { "distance": 1, "angle": 90, "alpha": 80, "blur": 5, "strength": 1.5 },
      "textFormat": { "font": "$TitleFont", "size": 20, "align": "center" },
      "format": "{{py:xvm.total_hp.ally}}"
    },
	// ?? ???????????
	"totalHP_enemy": { 
	  "$ref": { "path":"def.totalHP_ally" },
      "x": 232,
      "format": "{{py:xvm.total_hp.enemy}}"
    },
	// ??????? ???? ?? ???????
	"totalHP_avgDamage": { 
	  "enabled": false,
	  "updateEvent": "PY(ON_UPDATE_HP)",
      "x": -310,
	  "y": 40,
	  "width": 100,
      "height": 35,
	  "screenHAlign": "center",
	  "alpha":  "{{battletype!=regular?0|100}}",
	  "shadow": { "distance": 1, "angle": 90, "alpha": 80, "blur": 5, "strength": 1.5 },
      "textFormat": { "font": "$TitleFont", "color": "0xE0E06D", "size": 20, "align": "right" },
      "format": "{{py:xvm.total_hp.avgDamage('',{{hitlog.dmg-total}})?{{py:xvm.total_hp.avgDamage('',{{hitlog.dmg-total}})}}<img src='img://gui/maps/icons/vehParams/small/avgDamagePerMinute.png' width='26' height='26' align='baseline' vspace='-6'>}}"
    },
	// ???????? ??????
	"totalHP_mainGun": {  
	  "enabled": ${"../fdmod_ui_TeamHPbar.xc":"."},
	  "updateEvent": "PY(ON_UPDATE_HP)",
      "x": 310,
	  "y": 31,
	  "width": 100,
      "height": 35,
	  "screenHAlign": "center",
	  "alpha":  "{{battletype!=regular?0|100}}",
	  "shadow": { "distance": 1, "angle": 90, "alpha": 80, "blur": 5, "strength": 1.5 },
      "textFormat": { "font": "$TitleFont", "color": "0xE0E06D", "size": 20, "align": "left" },
      "format": "<img src='img://gui/maps/icons/achievement/mainGun.png' width='28' height='28' align='baseline' vspace='-21'>{{py:xvm.total_hp.mainGun('', {{hitlog.dmg-total}})}}"
	}
  }
}
