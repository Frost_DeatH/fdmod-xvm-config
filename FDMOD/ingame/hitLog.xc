﻿/**
 * ??? ????????? (??????? ????? ?????????)
 * ??????: "HEKPOMAHT"
 */
{
  "hitLog": {
    "showAllyDamage": true,
    "showSelfDamage": false,
    "log": {
      "addToEnd": false,
      "groupHitsByPlayer": true,
      "lines": 7,
      "moveInBattle": true,
      "x": 50,
      "y": 60,
      "dmg-kind": {
        "shot": "{{type-shell}}\t",
        "fire": "{{l10n:fire}}\t{{l10n:n/a}}\t",
        "ramming": "{{l10n:ramming}}\t{{l10n:n/a}}\t",
        "world_collision": "{{l10n:strike}}\t{{l10n:n/a}}\t",
        "overturn": "{{l10n:overturn}}\t{{l10n:n/a}}\t",
        "drowning": "{{l10n:drowning}}\t{{l10n:n/a}}\t",
        "art_attack": "{{l10n:Artattack}}\t{{l10n:n/a}}\t<font alpha='#E6' face='xvm' size='19'>&#x110;</font>",
        "air_strike": "{{l10n:Airstrike}}\t{{l10n:n/a}}\t<font alpha='#E6' face='xvm' size='19'>&#x111;</font>"
      },
      "c:dmg-kind": {
        "shot": "{{c:type-shell}}",
        "fire": "fire",
        "ramming": "ramming",
        "world_collision": "strike",
        "drowning": "drowning",
        "overturn": "overturn",
        "art_attack": "{{c:type-shell}}",
        "air_strike": "{{c:type-shell}}"
      },
      "vtype": {
        "HT": "&#x3F;",
        "MT": "&#x3B;",
        "LT": "&#x3A;",
        "TD": "&#x2E;",
        "SPG": "&#x2D;",
        "not_vehicle": ""
      },
      "c:team-dmg": {
        "ally-dmg": "#00EAFF",
        "enemy-dmg": "#{{alive?F0F0F0|FFD582}}",
        "unknown": "#{{alive?F0F0F0|FFD582}}"
      },
      "type-shell": {
        "armor_piercing": "{{l10n:{{blownup|pierced}}}}\t{{l10n:AP}}",
        "high_explosive": "{{l10n:{{splash-hit?close|{{blownup|debris}}}}}}\t{{l10n:HE}}",
        "armor_piercing_cr": "{{l10n:{{blownup|pierced}}}}\t{{l10n:APCR}}",
        "armor_piercing_he": "{{l10n:{{splash-hit?close|{{blownup|debris}}}}}}\t{{l10n:HESH}}",
        "hollow_charge": "{{l10n:{{blownup|pierced}}}}\t{{l10n:HEAT}}",
        "not_shell": ""
      },
      "c:type-shell": {
        "armor_piercing": "{{blownup|pierced}}",
        "high_explosive": "{{splash-hit|{{blownup|debris}}}}",
        "armor_piercing_cr": "{{blownup|pierced}}",
        "armor_piercing_he": "{{splash-hit|{{blownup|debris}}}}",
        "hollow_charge": "{{blownup|pierced}}",
        "not_shell": ""
      },
      "formatHistory": "<font color='{{c:team-dmg}}'><textformat tabstops='{{l10n:[26,58,136,172,191]}}'>&#xD7;{{n-player}}:\t-{{dmg-player%-4d}}\t<img src='cfg://FDMOD/img/log/{{c:team-dmg}}/{{c:dmg-kind}}.png' width='27' height='27' vspace='-9'>{{dmg-kind}}<font alpha='#E6' face='xvm' size='21'>{{vtype}}</font>\t{{vehicle}}</textformat></font>"
    },
    "logAlt": {
      "$ref": { "path": "hitLog.log" },
      "formatHistory": "<font color='{{c:team-dmg}}'><textformat tabstops='{{l10n:[26,58,136,172,191]}}'>&#xD7;{{n-player}}:\t-{{dmg-ratio-player%2d}}%\t<img src='cfg://FDMOD/img/log/{{c:team-dmg}}/{{c:dmg-kind}}.png' width='27' height='27' vspace='-9'>{{dmg-kind}}<font alpha='#E6' face='xvm' size='21'>{{vtype}}</font>\t{{name%.13s~..|{{vehicle}}}}</textformat></font>"
    },
    "logBackground": {
      "$ref": { "path": "hitLog.log" },
      "lines": 1,
      "c:team-dmg": {
        "ally-dmg": "#00EAFF",
        "enemy-dmg": "#{{alive?FFFFFF|FFD582}}",
        "unknown": "#{{alive?FFFFFF|FFD582}}"
      },
      "formatHistory": "{{py:xvm.numberShotsDealt>0?{{l10n:Shots}}: <font color='#FFFFFF'><b>{{py:xvm.numberShotsDealt}}</b></font>{{py:xvm.numberHitsDealt=0?&nbsp;&nbsp;&nbsp;}}}}{{py:xvm.numberHitsDealt>0? {{py:xvm.numberShotsDealt={{py:xvm.numberHitsDealt}}?&#61;|&#62;}} <font color='#FFFFFF'><b>{{py:xvm.numberHitsDealt}}</b></font>&nbsp;&nbsp;&nbsp;}}{{py:xvm.totalDamage>0?{{l10n:Damage}}: <font color='#{{py:xvm.hitAlly?00EAFF|FFD582}}' size='15'><b>{{py:xvm.totalDamage}}</b></font>&nbsp;&nbsp;&nbsp;{{l10n:Last}}|{{l10n:Team damage}}}}: <font color='{{c:team-dmg}}'><b>{{dmg}}</b></font>"
    },
    "logAltBackground": {
      "$ref": { "path": "hitLog.logBackground" }
    }
  }
}