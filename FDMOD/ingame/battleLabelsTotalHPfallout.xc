﻿/**
 * Battle inteface text fields.
 * ????????? ???? ??????? ??????????.
 */
{
    // ?????? ?? ?????? fallout
  "def": {
    // ??????? ????????
	"totalHP_bg": { 
      "enabled": true,
      "x": -2,
      "y": -1,
	  "width": 895,
      "height": 49,
      "screenHAlign": "center",
	  "format": "<img src='xvm://res/icons/totalHP/fallout/bg.png'>"
    },
	// ?????? ?? ?????????
	"totalHP_line_ally": { 
	  "enabled": true,
      "updateEvent": "PY(ON_UPDATE_HP)",
	  "x": "{{py:math.sum({{py:math.div({{py:xvm.total_hp.ally(68)}},-2)}}, -21)}}",
      "y": 10,
	  "width": "{{py:xvm.total_hp.ally(70)}}",
      "height": 11,
	  "bgColor": "0x5ACB00",
	  "alpha": 50,
      "screenHAlign": "center"	  
	},
	// ?????? ?? ???????????
	"totalHP_line_enemy": { 
	  "$ref": { "path":"def.totalHP_line_ally" },
	  "x": "{{py:math.sum({{py:math.div({{py:xvm.total_hp.enemy(68)}},2)}}, 21)}}",
	  "width": "{{py:xvm.total_hp.enemy(70)}}",
	  "bgColor": "0xF30900"
	},
	// ????? ?????????
	"totalHP_frags_ally": { 
	  "enabled": true,
      "updateEvent": "PY(ON_UPDATE_HP)",
      "x": -12,
      "y": 4,
	  "width": 30,
      "height": 30,
      "screenHAlign": "center",
	  "shadow": { "distance": 1, "angle": 90, "alpha": 80, "blur": 5, "strength": 1.5 },
      "textFormat": { "font": "$TitleFont", "size": 17, "align": "center" },
	  "format": "{{py:sp.allyFrags}}"
	},
	// ????? ???????????
	"totalHP_frags_enemy": { 
	  "$ref": { "path":"def.totalHP_frags_ally" },
      "x": 12,
      "format": "{{py:sp.enemyFrags}}"
	},
	// ??????? ???????? ?? ??
	"totalHP_Bg_advant": { 
	  "enabled": true,
	  "updateEvent": "PY(ON_UPDATE_HP)",
      "x": -2,
      "y": -1,
	  "width": 30,
	  "height": 30,
	  "screenHAlign": "center",
	  "shadow": { "distance": 1, "angle": 90, "alpha": 80, "blur": 5, "strength": 1.5 },
	  "textFormat": { "font": "$TitleFont", "size": 15, "align": "center" },
	  "format": "{{py:math.sub({{py:xvm.total_hp.ally}}, {{py:xvm.total_hp.enemy}})>0?<img src='xvm://res/icons/totalHP/fallout/ally_sing.png'>|<img src='xvm://res/icons/totalHP/fallout/enemy_sing.png'>}}"
 	  },
	  // ??????????? ???
	"totalHP_Bg_sing": { 
	  "enabled": true,
      "x": -2,
      "y": -1,
	  "width": 895,
      "height": 49,
      "screenHAlign": "center",
	  "format": "<img src='xvm://res/icons/totalHP/fallout/sing.png'>"
    },
	// ??????????? ??????
	"totalHP_sing": { 
	  "enabled": true,
      "updateEvent": "PY(ON_UPDATE_HP)",
      "x": 0,
      "y": 4,
	  "width": 30,
      "height": 30,
      "screenHAlign": "center",
	  "shadow": { "distance": 1, "angle": 90, "alpha": 80, "blur": 5, "strength": 1.5 },
      "textFormat": { "font": "$TitleFont", "size": 15, "align": "center" },
      "format": "<font size='15' color='{{py:math.sub({{py:xvm.total_hp.ally}}, {{py:xvm.total_hp.enemy}})>0?#5ACB00|#F30900}}'>|</font>"
    },
	// ??????? ?? ??
    "totalHP_advant": { 
	  "enabled": true,
      "updateEvent": "PY(ON_UPDATE_HP)",
      "x": 0,
      "y": 26,
	  "width": 160,
	  "height": 30,
	  "screenHAlign": "center",
	  "shadow": { "distance": 1, "angle": 90, "alpha": 80, "blur": 5, "strength": 1.5 },
      "textFormat": { "font": "$TitleFont", "size": 15, "align": "center" },
      "format": "<font size='18' color='{{py:math.sub({{py:xvm.total_hp.ally}}, {{py:xvm.total_hp.enemy}})>0?#5ACB00|#F30900}}'>{{py:math.sub({{py:xvm.total_hp.ally}}, {{py:xvm.total_hp.enemy}})}}</font>"
    },
	// ?? ?????????
	"totalHP_ally": {  
	  "enabled": true,
      "updateEvent": "PY(ON_UPDATE_HP)",
      "x": -115,
	  "y": 4,
	  "width": 70,
	  "height": 30,
      "screenHAlign": "center",
	  "shadow": { "distance": 1, "angle": 90, "alpha": 80, "blur": 5, "strength": 1.5 },
      "textFormat": { "font": "$TitleFont", "size": 15, "align": "center" },
      "format": "{{py:xvm.total_hp.ally}}"
    },
	// ?? ???????????
	"totalHP_enemy": { 
	  "$ref": { "path":"def.totalHP_ally" },
      "x": 115,
      "format": "{{py:xvm.total_hp.enemy}}"
    },
	// ??????? ???? ?? ???????
	"totalHP_avgDamage": { 
	  "enabled": false,
	  "updateEvent": "PY(ON_UPDATE_HP)",
      "x": -170,
	  "y": 2,
	  "width": 60,
      "height": 35,
	  "alpha":  "{{battletype!=regular?0|100}}",
	  "screenHAlign": "center",
	  "shadow": { "distance": 1, "angle": 90, "alpha": 80, "blur": 5, "strength": 1.5 },
      "textFormat": { "font": "$TitleFont", "color": "0xE0E06D", "size": 15, "align": "right" },
      "format": "{{py:xvm.total_hp.avgDamage('',{{hitlog.dmg-total}})?{{py:xvm.total_hp.avgDamage('',{{hitlog.dmg-total}})}}<img src='img://gui/maps/icons/vehParams/small/avgDamagePerMinute.png' width='22' height='22' align='baseline' vspace='-6'>}}"
    },
	// ???????? ??????
	"totalHP_mainGun": {  
	  "enabled": true,
	  "updateEvent": "PY(ON_UPDATE_HP)",
      "x": 170,
	  "y": 4,
	  "width": 60,
      "height": 35,
	  "alpha":  "{{battletype!=regular?0|100}}",
	  "screenHAlign": "center",
	  "shadow": { "distance": 1, "angle": 90, "alpha": 80, "blur": 5, "strength": 1.5 },
      "textFormat": { "font": "$TitleFont", "color": "0xE0E06D", "size": 15, "align": "left" },
      "format": "<img src='img://gui/maps/icons/achievement/mainGun.png' width='22' height='22' align='baseline' vspace='-21'>{{py:xvm.total_hp.mainGun('', {{hitlog.dmg-total}})}}"
	}
  }
}
