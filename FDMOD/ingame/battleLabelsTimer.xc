﻿{
  "def": {
    "battleTimer": { 
      "enabled": false,
      "updateEvent": "PY(ON_BATTLE_TIMER)",
      "x": 0,
      "y": -5,
      "width": 80,
      "height": 45,
      "screenHAlign": "right",
      "shadow": { "distance": 1, "angle": 90, "alpha": 80, "blur": 3, "strength": 2 },
      "textFormat": { "size": 26 },
      "format": "<font color='{{py:xvm.critTimeBT?#FE4D4D}}'>{{py:xvm.minutesBT%02d~:}}{{py:xvm.secondsBT%02d}}</font>"
    }
  }
}