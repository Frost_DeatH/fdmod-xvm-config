﻿/**
 * List of battle interface labels.
 * ?????? ????????? ????? ??????? ??????????.
 */
{
  "labels": {
    // Referenced labels. Note, that every custom field can be separate enabled or disabled by "enabled" switch in their settings
    // ???????????? ????????? ????. ???????? ????????, ??? ????????? ???? ????? ???????? ????????? ? ???????? ? ??????? "enabled" ? ?? ??????????
    "formats": [
      // Opravy modulov v damage paneli
      ${ "battleLabelsTemplates.xc":"def.repairTimeEngine" },
      ${ "battleLabelsTemplates.xc":"def.repairTimeGun" },
      ${ "battleLabelsTemplates.xc":"def.repairTimeTurret" },
      ${ "battleLabelsTemplates.xc":"def.repairTimeTracks" },
      ${ "battleLabelsTemplates.xc":"def.repairTimeSurveying" },
      ${ "battleLabelsTemplates.xc":"def.repairTimeRadio" },      
      //${ "battleLabelsTemplates.xc":"def.damageLog" },
      //${ "battleLabelsTemplates.xc":"def.lastHit" },
      //${ "battleLabelsTemplates.xc":"def.fire" },
      // Hitlog
      ${ "battleLabelsTemplates.xc":"def.hitLog.header" },
      ${ "battleLabelsTemplates.xc":"def.hitLog.body" },
      ${ "battleLabelsTemplates.xc": "def.damageLog.bg" },
      ${ "battleLabelsTemplates.xc": "def.damageLog.history" },
      ${ "battleLabelsTemplates.xc": "def.efficiency.bg" },
      ${ "battleLabelsTemplates.xc": "def.efficiency.data" },
      ${ "battleLabelsTemplates.xc": "def.efficiency.dataKey" },
      // ${ "battleLabelsTemplates.xc":"def.totalDamage" },
      // ${ "battleLabelsTemplates.xc":"def.totalDamageCount" },
      // ${ "battleLabelsTemplates.xc":"def.totalAssist" },
      // ${ "battleLabelsTemplates.xc":"def.totalBlocked" },
      // ${ "battleLabelsTemplates.xc":"def.totalBlockedCount" },
      // ${ "battleLabelsTemplates.xc":"def.detection" },
      // ${ "battleLabelsTemplates.xc":"def.totalStun" },
      // ${ "battleLabelsTemplates.xc":"def.totalStunCount" },
      // ??
      ${ "battleLabelsTemplates.xc":"def.TotalEfficiencyBg" },      
      ${ "battleLabelsTemplates.xc":"def.TotalEfficiency" },
      ${ "battleLabelsTemplates.xc":"def.teamRatingLogo"},
      ${ "battleLabelsTemplates.xc":"def.teamRatingWinChance"},
      ${ "battleLabelsTemplates.xc":"def.teamRating"},
      // Šanca na výhru
      ${ "battleLabelsTeamRating.xc":"def.teamWinChance" },
      ${ "battleLabelsTeamRating.xc":"def.altMessage" },
      ${ "battleLabelsTeamRating.xc":"def.teamRating" },
      ${ "battleLabelsTeamRating.xc":"def.teamHP" },
      // WN8 live
      ${ "battleLabelsTemplates.xc":"def.battleEfficiency" },
      // Debug panel
      ${ "battleLabelsDebugPanel3.xc":"def.debugPanel" },
      ${ "battleLabelsDebugPanel3.xc":"def.debugPanelpingBar" },
      ${ "battleLabelsDebugPanel3.xc":"def.debugPanelClock " },
      // Tímový HP bar
      ${ "battleLabelsTotalHPArm.xc":"def.totalHP_bg" },
      ${ "battleLabelsTotalHPArm.xc":"def.totalHP_Bg_ally" },
      ${ "battleLabelsTotalHPArm.xc":"def.totalHP_Bg_enemy" },
      ${ "battleLabelsTotalHPArm.xc":"def.totalHP_line_ally" },
      ${ "battleLabelsTotalHPArm.xc":"def.totalHP_line_enemy" },  
      ${ "battleLabelsTotalHPArm.xc":"def.totalHP_ally" },
      ${ "battleLabelsTotalHPArm.xc":"def.totalHP_enemy" },
      ${ "battleLabelsTotalHPArm.xc":"def.totalHP_frags_ally" },
      ${ "battleLabelsTotalHPArm.xc":"def.totalHP_frags_enemy" },
      ${ "battleLabelsTotalHPArm.xc":"def.totalHP_advant" },  
      ${ "battleLabelsTotalHPArm.xc":"def.totalHP_sing" },
      // Opravy cez tlačidlo CTRL 
      ${ "battleLabelsRepairControl.xc":"repairCtrlEngine" },
      ${ "battleLabelsRepairControl.xc":"repairCtrlAmmoBay" },
      ${ "battleLabelsRepairControl.xc":"repairCtrlGun" },
      ${ "battleLabelsRepairControl.xc":"repairCtrlTurret" },
      ${ "battleLabelsRepairControl.xc":"repairCtrlTracks" },
      ${ "battleLabelsRepairControl.xc":"repairCtrlSurveying" },
      ${ "battleLabelsRepairControl.xc":"repairCtrlRadio" },
      ${ "battleLabelsRepairControl.xc":"repairCtrlFuelTank" },
      ${ "battleLabelsRepairControl.xc":"healCtrlCommander" },
      ${ "battleLabelsRepairControl.xc":"healCtrlRadioman" },
      ${ "battleLabelsRepairControl.xc":"healCtrlDriver" },
      ${ "battleLabelsRepairControl.xc":"healCtrlGunner" },
      ${ "battleLabelsRepairControl.xc":"healCtrlLoader" }
      //${ "battleLabelsTemplates.xc":"def.test" }
      //${ "battleLabelsTemplates.xc":"def.test2" }
    ]
  }
}
