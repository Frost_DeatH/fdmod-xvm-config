﻿/**
 * ?????? ???????
 * "{{py:sum({{py:xvm.screenVCenter}},362)}}" "{{pp.mode=0?{{battletype-key=epic_battle?20|111}}}}",
 */
{
  "captureBar": {
    "y": 65,
    "distanceOffset": 0,
    "enemy": {
      "title": {
        "x": 0, "y": 0,
        "shadow": { "alpha": 35, "angle": 90, "blur": 3, "color": "0x000000", "distance": 1, "enabled": true, "strength": 1 },
        "format": "<font color='#FFFFFF' face='mono' size='13'><b>{{cap.points%2d}}</b>%</font>    {{battletype-key=cybersport?{{l10n:Base}}}}",
        "done": "<font color='#F0F0F0' face='$FieldFont' size='13'>{{l10n:Base captured by enemies}}!</font>"
      },
      "players": {
        "x": -225, "y": 0,
        "shadow": ${ "captureBar.enemy.title.shadow" },
        "format": "<font color='#F0F0F0' face='xvm' size='15'>&#x113;</font>  <font color='#FFFFFF' face='$FieldFont' size='13'><b>{{cap.tanks}}</b></font>",
        "done": "<font color='#F0F0F0' face='xvm' size='15'>&#x113;</font>  <font color='#FFFFFF' face='$FieldFont' size='13'><b>{{cap.tanks}}</b></font>"
      },
      "timer": {
        "x": 200, "y": 0,
        "shadow": ${ "captureBar.enemy.title.shadow" },
        "format": "<font color='#F0F0F0' face='xvm' size='15'>&#x114;</font>  <font color='#FFFFFF' face='$FieldFont' size='13'><b>{{cap.time}}</b></font>",
        "done": "<font color='#F0F0F0' face='xvm' size='15'>&#x114;</font>  <font color='#FFFFFF' face='$FieldFont' size='13'><b>{{cap.time}}</b></font>"
      },
      "background": {
        "x": 0, "y": -15,
        "format": "<img src='cfg://FDMOD/img/captureBar/bg.png' width='388' height='48'>",
        "done": "<img src='cfg://FDMOD/img/captureBar/enemy.png' width='388' height='48'>"
      }
    },
    "ally": {
      "title": {
        "$ref": { "path": "captureBar.enemy.title" },
        "format": "<font color='#FFFFFF' face='mono' size='13'><b>{{cap.points%2d}}</b>%</font>    {{battletype-key=cybersport?{{l10n:Base}}}}",
        "done": "<font color='#F0F0F0' face='$FieldFont' size='13'>{{l10n:Base captured by allies}}!</font>"
      },
      "players": ${ "captureBar.enemy.players" },
      "timer": ${ "captureBar.enemy.timer" },
      "background": {
        "x": 0, "y": -15,
        "format": "<img src='cfg://FDMOD/img/captureBar/bg.png' width='388' height='48'>",
        "done": "<img src='cfg://FDMOD/img/captureBar/ally.png' width='388' height='48'>"
      }
    }
  }
}