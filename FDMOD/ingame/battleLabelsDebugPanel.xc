﻿{
  "def": {
    "debugPanel": {
      "enabled": false,
      "updateEvent": "ON_EVERY_FRAME",
      "x": 2,
      "y": 0,
      "align": "left",
      "screenHAlign": "left",
      "screenVAlign": "top",
      "shadow": { "distance": 0, "angle": 45, "alpha": 90, "blur": 4, "strength": 2 },
      "textFormat": { "font": "Calibri", "color": "0xEEEEEE", "size": 18, "align": "left" },
      "format": "<textformat tabstops='[70]'><b><font color='{{py:xvm.lag?#ff0000}}'>FPS: </font><font color='{{py:xvm.fps>45?#60FF00|{{py:xvm.fps>30?#F8F400|{{py:xvm.fps>20?#FE7903|#FE0E00}}}}}}'>{{py:xvm.fps%3d}}</font><tab><font color='{{py:xvm.lag?#ff0000}}'>PING: </font><font color='{{py:xvm.ping>160?#FE0E00|{{py:xvm.ping>100?#FE7903|{{py:xvm.ping>80?#F8F400|#60FF00}}}}}}'>{{py:xvm.ping%3d}}</font></b></textformat>"
    }
  }
}