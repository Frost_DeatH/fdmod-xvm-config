﻿/**
 * ??? ?????????? ?????????
 * ??????: "HEKPOMAHT"
 */
{
  "damageLog": {
    "disabledDetailStats": true,
    "disabledSummaryStats": true,
    "log": {
      "moveInBattle": true,
      "showHitNoDamage": true,
      "groupDamagesFromFire": true,
      "groupDamagesFromRamming_WorldCollision": true,
      "groupDamageFromArtAndAirstrike": true,
      "dmg-kind": {
        "shot": "{{l10n:{{hit-effects}}}}\t\t{{.damageLog.log.name.{{type-shell-key}}}}\t",
        "fire": "{{l10n:fire}}\t&#x7E;{{fire-duration%2.01f}} {{l10n:s}}\t\t",
        "ramming": "{{l10n:ramming}}\t\t{{l10n:n/a}}\t",
        "world_collision": "{{l10n:strike}}\t\t{{l10n:n/a}}\t",
        "overturn": "{{l10n:overturn}}\t\t{{l10n:n/a}}\t",
        "drowning": "{{l10n:drowning}}\t\t{{l10n:n/a}}\t",
        "art_attack": "{{l10n:{{hit-effects}}}}\t\t{{l10n:HE}}\t<font alpha='#E6' face='xvm' size='19'>&#x110;</font>\t{{l10n:Artattack}}",
        "air_strike": "{{l10n:{{hit-effects}}}}\t\t{{l10n:PTAB}}\t<font alpha='#E6' face='xvm' size='19'>&#x111;</font>\t{{l10n:Airstrike}}"
      },
      "c:dmg-kind": {
        "shot": "{{my-blownup|{{c:type-shell}}}}",
        "fire": "fire",
        "ramming": "ramming",
        "world_collision": "strike",
        "drowning": "drowning",
        "overturn": "overturn",
        "art_attack": "{{my-blownup|{{c:type-shell}}}}",
        "air_strike": "{{my-blownup|{{c:type-shell}}}}"
      },
      "splash-hit": {
        "splash": "splash",
        "no-splash": ""
      },
      "name": {
        "armor_piercing": "{{l10n:AP}}",
        "high_explosive": "{{l10n:HE}}",
        "armor_piercing_cr": "{{l10n:APCR}}",
        "armor_piercing_he": "{{l10n:HESH}}",
        "hollow_charge": "{{l10n:HEAT}}",
        "not_shell": ""
      },
      "c:type-shell": {
        "armor_piercing": "{{dmg=0?nullpierced|pierced}}",
        "high_explosive": "{{splash-hit|{{dmg=0?nulldebris|debris}}}}",
        "armor_piercing_cr": "{{dmg=0?nullpierced|pierced}}",
        "armor_piercing_he": "{{splash-hit|{{dmg=0?nulldebris|debris}}}}",
        "hollow_charge": "{{dmg=0?nullpierced|pierced}}",
        "not_shell": "{{splash-hit|{{dmg=0?nulldebris|debris}}}}"
      },
      "type-shell": {
        "armor_piercing": "pierced",
        "high_explosive": "{{splash-hit|debris}}",
        "armor_piercing_cr": "pierced",
        "armor_piercing_he": "{{splash-hit|debris}}",
        "hollow_charge": "pierced",
        "not_shell": "{{splash-hit|debris}}"
      },
      "vtype": {
        "HT": "<font alpha='#E6' face='xvm'>&#x3F;</font>",
        "MT": "<font alpha='#E6' face='xvm'>&#x3B;</font>",
        "LT": "<font alpha='#E6' face='xvm'>&#x3A;</font>",
        "TD": "<font alpha='#E6' face='xvm'>&#x2E;</font>",
        "SPG": "<font alpha='#E6' face='xvm'>&#x2D;</font>",
        "not_vehicle": ""
      },
      "hit-effects": {
        "armor_pierced": "{{my-blownup|{{type-shell}}}}",
        "intermediate_ricochet": "ricochet",
        "final_ricochet": "ricochet",
        "armor_not_pierced": "bounce",
        "armor_pierced_no_damage": "{{splash-hit|blocked}}",
        "unknown": "{{splash-hit|blocked}}"
      },
      "critical-hit": {
        "critical": "*",
        "no-critical": ""
      },
      "crit-device": {
        "engine_crit": "{{l10n:critical}}",
        "ammo_bay_crit": "{{l10n:critical}}",
        "fuel_tank_crit": "{{l10n:critical}}",
        "radio_crit": "{{l10n:critical}}",
        "left_track_crit": "{{l10n:critical}}",
        "right_track_crit": "{{l10n:critical}}",
        "gun_crit": "{{l10n:critical}}",
        "turret_rotator_crit": "{{l10n:critical}}",
        "surveying_device_crit": "{{l10n:critical}}",
        "engine_destr": "{{l10n:critical}}",
        "ammo_bay_destr": "{{l10n:critical}}",
        "fuel_tank_destr": "{{l10n:critical}}",
        "radio_destr": "{{l10n:critical}}",
        "left_track_destr": "{{l10n:critical}}",
        "right_track_destr": "{{l10n:critical}}",
        "gun_destr": "{{l10n:critical}}",
        "turret_rotator_destr": "{{l10n:critical}}",
        "surveying_device_destr": "{{l10n:critical}}",
        "commander": "{{l10n:critical}}",
        "driver": "{{l10n:critical}}",
        "radioman": "{{l10n:critical}}",
        "gunner": "{{l10n:critical}}",
        "loader": "{{l10n:critical}}",
        "no-critical": ""
      },
      "c:team-dmg": {
        "ally-dmg": "#00EAFF",
        "enemy-dmg": "{{c:costShell}}",
        "player": "#B9FFA1",
        "unknown": "#F0F0F0"
      },
      "costShell": {
        "gold-shell": "<img src='cfg://FDMOD/img/log/gold.png' width='47' height='88' vspace='-18'>",
        "silver-shell": "<img src='cfg://FDMOD/img/log/none.png' width='1' height='88' vspace='-18'>",
        "unknown": "<img src='cfg://FDMODT/img/log/none.png' width='1' height='88' vspace='-18'>"
      },
      "c:costShell": {
        "gold-shell": "#FFD582",
        "silver-shell": "#F0F0F0",
        "unknown": "#F0F0F0"
      },
      "formatHistory": "<font color='{{c:team-dmg}}'><textformat tabstops='{{l10n:[26,58,129,136,172,191]}}'>{{my-alive?<font face='mono'>{{number%02d}}</font>{{critical-hit}}|<font alpha='#E6' face='xvm' size='15'>&#x2B;</font>}}\t{{dmg=0?<img src='cfg://FDMOD/img/log/{{c:team-dmg}}/arrow.png' width='27' height='27' vspace='-9'>|-{{dmg}}}}\t<img src='cfg://FDMOD/img/log/{{c:team-dmg}}/{{c:dmg-kind}}.png' width='27' height='27' vspace='-9'>{{dmg-kind}}<font size='21'>{{vtype}}</font>\t{{vehicle}}</textformat></font>"
    },
    "logBackground": {
      "$ref": { "path": "damageLog.log" },
      "formatHistory": "<textformat leading='-14'><img src='cfg://FDMOD/img/log/bg{{dmg=0?No|}}Damage.png' width='172' height='34'></textformat>"
    },
    "logAlt": {
      "$ref": { "path": "damageLog.log" },
      "dmg-kind": {
        "shot": "{{{{gun-caliber%.1f}}={{gun-caliber%.0f}}?{{gun-caliber%.0f}}|{{gun-caliber%.1f}}}} {{l10n:mm}}\t\t{{.damageLog.log.name.{{type-shell-key}}}}\t",
        "fire": "{{l10n:fire}}\t&#x7E;{{fire-duration%2.01f}} {{l10n:s}}\t\t",
        "ramming": "{{l10n:ramming}}\t\t{{l10n:n/a}}\t",
        "world_collision": "{{l10n:strike}}\t\t{{l10n:n/a}}\t",
        "overturn": "{{l10n:overturn}}\t\t{{l10n:n/a}}\t",
        "drowning": "{{l10n:drowning}}\t\t{{l10n:n/a}}\t",
        "art_attack": "{{{{gun-caliber%.1f}}={{gun-caliber%.0f}}?{{gun-caliber%.0f}}|{{gun-caliber%.1f}}}} {{l10n:mm}}\t\t{{l10n:HE}}\t<font alpha='#E6' face='xvm' size='19'>&#x110;</font>\t{{l10n:Artattack}}",
        "air_strike": "{{{{gun-caliber%.1f}}={{gun-caliber%.0f}}?{{gun-caliber%.0f}}|{{gun-caliber%.1f}}}} {{l10n:mm}}\t\t{{l10n:PTAB}}\t<font alpha='#E6' face='xvm' size='19'>&#x111;</font>\t{{l10n:Airstrike}}"
      },
      "formatHistory": "<font color='{{c:team-dmg}}'><textformat tabstops='{{l10n:[26,58,129,136,172,191]}}'>{{my-alive?<font face='mono'>{{number%02d}}</font>{{critical-hit}}|<font alpha='#E6' face='xvm' size='15'>&#x2B;</font>}}\t{{dmg=0?<img src='cfg://FDMOD/img/log/{{c:team-dmg}}/arrow.png' width='27' height='27' vspace='-9'>|-{{dmg-ratio%d%%}}}}\t<img src='cfg://FDMOD/img/log/{{c:team-dmg}}/{{c:dmg-kind}}.png' width='27' height='27' vspace='-9'>{{dmg-kind}}<font size='21'>{{vtype}}</font>\t{{name%.13s~..}}</textformat></font>"
    },
    "logAltBackground": {
      "$ref": { "path": "damageLog.logBackground" }
    },
    "lastHit": {
      "$ref": { "path": "damageLog.log" },
      "timeDisplayLastHit": "{{c:dmg-kind}}",
      "dmg-kind": {
        "shot": "<font color='#E11F11'>{{my-alive?|{{l10n:{{my-blownup?Blown-Up|Vehicle destroyed}}}}}}</font>\n{{hit-effects}} <font color='{{c:costShell}}'>{{.damageLog.log.name.{{type-shell-key}}}}</font>{{costShell}}\n<font color='{{c:team-dmg}}'>{{vtype}} <font size='35'>{{vehicle}}</font></font>",
        "fire": "<textformat leading='-1'><font color='#E11F11'>{{my-alive?<font color='#FF6600'>{{l10n:Vehicle on fire}}</font>|{{l10n:Vehicle burnt out}}}}</font>\n</textformat><textformat leading='-12'>-{{dmg}}\n<font color='{{c:team-dmg}}'>{{vtype}} <font size='35'>{{vehicle}}</font></font></textformat>",
        "ramming": "<textformat leading='-1'><font color='#E11F11'>{{my-alive?|{{l10n:Vehicle destroyed}}}}</font>\n</textformat><textformat leading='-12'>{{dmg>0?-{{dmg}}  <font color='{{crit-device?#E11F11}}'>{{l10n:ramming}}</font>|{{l10n:Ramming critical}}}}\n<font color='{{c:team-dmg}}'>{{vtype}} <font size='35'>{{vehicle}}</font></font></textformat>",
        "world_collision": "<textformat leading='-1'><font color='#E11F11'>{{my-alive?|{{l10n:Vehicle crashed}}}}</font>\n</textformat><textformat leading='-12'>{{dmg>0?-{{dmg}}  <font color='{{crit-device?#E11F11}}'>{{l10n:crash}}</font>|{{l10n:Strike critical}}}}\n<font color='{{c:team-dmg}}'>{{vtype}} <font size='35'>{{vehicle}}</font></font></textformat>",
        "overturn": "<textformat leading='-1'><font color='#E11F11'>{{l10n:Vehicle destroyed}}</font>\n</textformat><textformat leading='-12'>-{{dmg}}  {{l10n:overturn}}\n<font color='{{c:team-dmg}}'>{{vtype}} <font size='35'>{{vehicle}}</font></font></textformat>",
        "drowning": "<textformat leading='-1'><font color='#E11F11'>{{l10n:Vehicle drowned}}</font>\n<font color='{{c:team-dmg}}'>{{vtype}} <font size='35'>{{vehicle}}</font></font></textformat>",
        "art_attack": "<font color='#E11F11'>{{my-alive?|{{l10n:{{my-blownup?Blown-Up|Vehicle destroyed}}}}}}</font>\n{{hit-effects}} {{l10n:HE}}\n<font color='{{c:team-dmg}}'><font face='xvm'>&#x110;</font>  <font size='35'>{{l10n:Artattack}}</font></font>",
        "air_strike": "<font color='#E11F11'>{{my-alive?|{{l10n:{{my-blownup?Blown-Up|Vehicle destroyed}}}}}}</font>\n{{hit-effects}} {{l10n:PTAB}}\n<font color='{{c:team-dmg}}'><font face='xvm'>&#x111;</font>  <font size='35'>{{l10n:Airstrike}}</font></font>"
      },
      "c:dmg-kind": {
        "shot": "10",
        "fire": "{{my-alive?1|10}}",
        "ramming": "10",
        "world_collision": "10",
        "drowning": "10",
        "overturn": "10",
        "art_attack": "10",
        "air_strike": "10"
      },
      "critical-hit": {
        "critical": "<font color='#E11F11'>{{py:capitalize('{{l10n:critical}}')}}</font> {{splash-hit|{{l10n:module}}}}",
        "no-critical": "{{py:capitalize('{{l10n:blocked}}')}} "
      },
      "splash-hit": {
        "splash": "<font face='xvm' size='81'>&#x117;</font>",
        "no-splash": ""
      },
      "hit-effects": {
        "armor_pierced": "-{{dmg}} <font color='#E11F11'>{{crit-device}}</font> {{splash-hit}}",
        "intermediate_ricochet": "{{py:capitalize('{{l10n:ricochet}}')}} ",
        "final_ricochet": "{{py:capitalize('{{l10n:ricochet}}')}} ",
        "armor_not_pierced": "{{py:capitalize('{{l10n:bounce}}')}} ",
        "armor_pierced_no_damage": "{{critical-hit}}",
        "unknown": "{{critical-hit}}"
      },
      "c:team-dmg": {
        "ally-dmg": "#00EAFF",
        "enemy-dmg": "#FF8518",
        "player": "#B9FFA1",
        "unknown": "#FF8518"
      },
      "formatLastHit": "{{dmg-kind}}"
    }
  }
}