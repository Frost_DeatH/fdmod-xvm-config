﻿{
  "safeShot": {
    "enabled": ${"../fdmod_ui_Safeshot.xc":"."},
    "wasteShotBlock": false,
    "teamShotBlock": true,
    "teamKillerShotUnblock": true,
    "deadShotBlock": true,
    "deadShotBlockTimeOut": 2,
    "clientMessages": {
      "wasteShotBlockedMessage": "Waste shot blocked!",
      "teamShotBlockedMessage": "Team shot blocked!",
      "deadShotBlockedMessage": "Dead shot blocked!"
    },
    "chatMessages": {
      //{{target-name}}, {{target-vehicle}}
      "teamShotBlockedMessage": "[{{target-vehicle}}], u block me!"
    },
    "disableKey": 88,
    "onHold": false,
    "disableMessage": true
  }
}