﻿/**
 * Battle inteface text fields.
 * ????????? ???? ??????? ??????????.
 */
{
  "def": {
    "textFieldShadow": { "enabled": true, "color": "0x000000", "alpha": 100, "blur": 4, "strength": 1, "distance": 0, "angle": 0 },
    "hitLog": {
      "header": {
        "enabled": ${ "../@xvm.xc": "module-off.hitLog" },
        "updateEvent": "ON_PANEL_MODE_CHANGED, PY(ON_HIT_LOG), PY(ON_TOTAL_EFFICIENCY)",
        "x": "{{battletype-key=epic_battle?255|{{pp.mode=0?5|{{py:sum({{pp.widthLeft}},125)}}}}}}", "y": "{{battletype-key=epic_battle?-01|-01}}", "width": 300, "height": 30,
        "shadow": { "alpha": 65, "angle": 90, "blur": 4.1, "color": "0x000000", "distance": 1, "strength": 1.4 },
        "textFormat": { "color": "0xF0F0F0", "font": "$FieldFont", "size": 13, "valign": "bottom" },
        "format": "{{py:xvm.totalDamage=0?{{py:xvm.dmgAlly?{{py:xvm.hitLog.log.bg}}|{{py:xvm.numberShotsDealt>0?{{l10n:Shots}}: <b>{{py:xvm.numberShotsDealt}}</b>}}}}|{{py:xvm.hitLog.log.bg}}}}"
      },
      "body": {
        "$ref": { "path": "def.hitLog.header" },
        "updateEvent": "ON_PANEL_MODE_CHANGED, PY(ON_HIT_LOG)",
        "x": ${ "def.hitLog.header.x" }, "y": "{{battletype-key=epic_battle?24|24}}", "width": 380, "height": 222,
        "textFormat": { "color": "0xF0F0F0", "font": "$FieldFont", "leading": -8, "size": 13 },
        "format": "{{py:xvm.hitLog.log}}"
      }
    },
    // Rewritable timer format
    // ???????????????? ?????? ???????
    "repairTimeItem": {
      "width": 47,
      "height": 40,
      "screenHAlign": "left",
      "screenVAlign": "bottom",
      "shadow": { "distance": 1, "angle": 90, "alpha": 90, "blur": 5, "strength": 4 },
      "textFormat": { "color": "0xF4EFE8", "size": 17, "align": "center", "valign": "center" }
    },
    // Repair timer for engine
    // ?????? ??????? ?????????
    "repairTimeEngine": {
      "$ref": { "path":"def.repairTimeItem" },
      "enabled": true,
      "updateEvent": "PY(ON_ENGINE_UPDATE)",
      "x": 4,
      "y": -147,
      "format": "<b>{{py:repairTimeEngine%0.1f}}</b>"
    },
    // Repair timer for gun
    // ?????? ??????? ??????
    "repairTimeGun": {
      "$ref": { "path":"def.repairTimeItem" },
      "enabled": true,
      "updateEvent": "PY(ON_GUN_UPDATE)",
      "x": 4,
      "y": -69,
      "format": "<b>{{py:repairTimeGun%0.1f}}</b>"
    },
    // Repair timer for turret rotator
    // ?????? ??????? ????????? ???????? ?????
    "repairTimeTurret": {
      "$ref": { "path":"def.repairTimeItem" },
      "enabled": true,
      "updateEvent": "PY(ON_TURRET_UPDATE)",
      "x": 4,
      "y": -30,
      "format": "<b>{{py:repairTimeTurret%0.1f}}</b>"
    },
    // Repair timer for tracks
    // ?????? ??????? ?????? ???????
    "repairTimeTracks": {
      "$ref": { "path":"def.repairTimeItem" },
      "enabled": true,
      "updateEvent": "PY(ON_TRACKS_UPDATE)",
      "x": 177,
      "y": -147,
      "format": "<b>{{py:repairTimeTracks%0.1f}}</b>"
    },
    // Repair timer for surveying device
    // ?????? ??????? ???????? ??????????
    "repairTimeSurveying": {
      "$ref": { "path":"def.repairTimeItem" },
      "enabled": true,
      "updateEvent": "PY(ON_SURVEYING_UPDATE)",
      "x": 177,
      "y": -108,
      "format": "<b>{{py:repairTimeSurveying%0.1f}}</b>"
    },
    // Repair timer for radio
    "repairTimeRadio": {
       "$ref": { "path":"def.repairTimeItem" },
       "enabled": true,
       "updateEvent": "PY(ON_RADIO_UPDATE)",
       "x": 177,
       "y": -69,
       "format": "<b>{{py:repairTimeRadio%0.1f}}</b>"
    },   
    // Damage log (see damageLog.xc).
    // ??? ??????????? ????? (??. damageLog.xc).
    "damageLog": {
      "history": {
        "enabled": ${ "../@xvm.xc": "module-off.damageLog" },
        "updateEvent": "PY(ON_HIT)",
        "x": 239, "y": "{{py:xvm.screenWidth<1600?-39|11}}", "width": 380, "height": "{{py:xvm.screenWidth<1600?175|225}}", "screenVAlign": "bottom",
        "shadow": { "alpha": 65, "angle": 90, "blur": 4.1, "color": "0x000000", "distance": 1, "strength": 1.4 },
        "textFormat": { "color": "0xF0F0F0", "font": "$FieldFont", "leading": -7, "size": 13 },
        "format": "{{py:xvm.damageLog.log}}"
      },
      "bg": {
        "$ref": { "path": "def.damageLog.history" },
        "x": 213, "y": "{{py:xvm.screenWidth<1600?-43|7}}",
        "format": "{{py:xvm.damageLog.log.bg}}"
      }
    },
    "efficiency": {
      "data": {
        "enabled": ${ "../@xvm.xc": "module-off.damageLog" },
        "updateEvent": "PY(ON_IMPACT), PY(ON_TOTAL_EFFICIENCY)",
        "x": 232, "y": -200, "width": 160, "height": 80, "screenVAlign": "bottom", "hotKeyCode": 56, "visibleOnHotKey": false, "onHold": true,
        "alpha": "{{py:sum({{py:xvm.totalAssist}},{{py:xvm.totalStun}},{{py:xvm.detection}},{{py:xvm.damagesSquad}})>0?100|{{py:xvm.isImpact?100|0}}}}",
        "shadow": { "alpha": 65, "angle": 90, "blur": 4.1, "color": "0x000000", "distance": 1, "strength": 1.4 },
        "textFormat": { "color": "0xF0F0F0", "font": "$FieldFont", "leading": -3, "size": 15 },
        "format": "<img src='cfg://FDMOD/img/log/totalAssist.png' width='35' height='25' vspace='-6'>{{py:xvm.totalAssist}}\n<img src='cfg://FDMOD/img/log/total{{py:xvm.isStuns?Stun|Blocked}}.png' width='35' height='25' vspace='-6'>{{py:xvm.isStuns?{{py:xvm.totalStun}}|{{py:xvm.totalBlocked}}}}\n<img src='cfg://FDMOD/img/log/detection.png' width='35' height='25' vspace='-6'>{{py:xvm.detection}} {{py:xvm.detection>8?({{l10n:scout}})}}"
      },
      "dataKey": {
        "$ref": { "path": "def.efficiency.data" },
        "hotKeyCode": 56, "visibleOnHotKey": true, "onHold": true,
        "format": "<img src='cfg://FDMOD/img/log/totalDamagesAssist.png' width='35' height='25' vspace='-6'>{{py:xvm.totalDamagesAssist}}\n<img src='cfg://FDMOD/img/log/damagesSquad.png' width='35' height='25' vspace='-6'>{{py:xvm.damagesSquad}}\n<img src='cfg://FDMOD/img/log/totalDamagesSquad.png' width='35' height='25' vspace='-6'>{{py:xvm.totalDamagesSquad}}"
      },
      "bg": {
        "$ref": { "path": "def.efficiency.data" },
        "x": 225, "y": -202, "hotKeyCode": null,
        "format": "<img src='cfg://FDMOD/img/log/bg.png' width='160' height='78'>"
      }
    },
    // Total damage.
    // ?????????? ????.
    "totalDamage": {
      "enabled": true,
      "hotKeyCode": 56, "onHold": "true", "visibleOnHotKey": false,
      "updateEvent": "PY(ON_TOTAL_EFFICIENCY), ON_PANEL_MODE_CHANGED",
      "x": "{{py:sum({{pp.widthLeft}},33)}}",
      "y": 35,
      "alpha": "{{py:xvm.totalDamage>0?100|{{py:xvm.numberShotsDealt>0?100|60}}}}",
      "width": 70,
      "height": 25,
      "textFormat": { "color": "{{py:xvm.totalDamage>0?{{py:xvm.totalDamageColor}}|{{py:xvm.numberShotsDealt>0?{{py:xvm.totalDamageColor}}|0xFFFFFF}}}}", "size": 15 },
      "format": "<font face='XVMSymbol' size='21' color='#F2F2F2'>&#x10D;</font> <b>{{py:xvm.totalDamage}}</b>",
      "shadow": ${ "def.textFieldShadow" }
    },
    // Total damage count.
    // ?????????? ???? ???????.
    "totalDamageCount": {
      "enabled": true,
      "hotKeyCode": 56, "onHold": "true", "visibleOnHotKey": true,
      "updateEvent": "PY(ON_TOTAL_EFFICIENCY), ON_PANEL_MODE_CHANGED",
      "x": "{{py:sum({{pp.widthLeft}},33)}}",
      "y": 35,
      "alpha": "{{py:xvm.numberShotsDealt>0?100|60}}",
      "width": 70,
      "height": 25,
      "textFormat": { "color": "{{py:xvm.numberShotsDealt>0?0xFFC363|0xFFFFFF}}", "size": 15 },
      "format": "<font face='XVMSymbol' size='21' color='#F2F2F2'>&#x10D;</font> <b>{{py:xvm.numberHitsDealt}} / {{py:xvm.numberShotsDealt}}</b>",
      "shadow": ${ "def.textFieldShadow" }
    },
    // Assisted damage.
    // ???? ?? ???????.
    "totalAssist": {
      "enabled": true,
      "updateEvent": "PY(ON_TOTAL_EFFICIENCY), ON_PANEL_MODE_CHANGED",
      "x": "{{py:sum({{pp.widthLeft}},98)}}",
      "y": 35,
      "alpha": "{{py:xvm.totalAssist>0?100|60}}",
      "width": 70,
      "height": 25,
      "textFormat": { "color": "{{py:xvm.totalAssist>0?0xFFC363|0xFFFFFF}}", "size": 15 },
      "format": "<font face='XVMSymbol' size='21' color='#F2F2F2'>&#x10B;</font> <b>{{py:xvm.totalAssist}}</b>",
      "shadow": ${ "def.textFieldShadow" }
    },
    // Blocked damage.
    // ??????????????? ????.
    "totalBlocked": {
      "enabled": true,
      "hotKeyCode": 56, "onHold": "true", "visibleOnHotKey": false,
      "updateEvent": "PY(ON_TOTAL_EFFICIENCY), ON_PANEL_MODE_CHANGED",
      "x": "{{py:sum({{pp.widthLeft}},168)}}",
      "y": 35,
      "alpha": "{{py:xvm.numberShotsReceived>0?100|60}}",
      "width": 70,
      "height": 25,
      "textFormat": { "color": "{{py:xvm.numberShotsReceived>0?0xFFC363|0xFFFFFF}}", "size": 15 },
      "format": "<font face='XVMSymbol' size='21' color='#F2F2F2'>&#x10C;</font> <b>{{py:xvm.totalBlocked}}</b>",
      "shadow": ${ "def.textFieldShadow" }
    },
    // Blocked damage count.
    // ??????????????? ???? ???????.
    "totalBlockedCount": {
      "enabled": true,
      "hotKeyCode": 56, "onHold": "true", "visibleOnHotKey": true,
      "updateEvent": "PY(ON_TOTAL_EFFICIENCY), ON_PANEL_MODE_CHANGED",
      "x": "{{py:sum({{pp.widthLeft}},168)}}",
      "y": 35,
      "alpha": "{{py:xvm.numberShotsReceived>0?100|60}}",
      "width": 70,
      "height": 25,
      "textFormat": { "color": "{{py:xvm.numberShotsReceived>0?0xFFC363|0xFFFFFF}}", "size": 15 },
      "format": "<font face='XVMSymbol' size='21' color='#F2F2F2'>&#x10C;</font> <b>{{py:xvm.numberHitsBlocked}} / {{py:xvm.numberShotsReceived}}</b>",
      "shadow": ${ "def.textFieldShadow" }
    },
    // Detected enemies.
    // ???????????? ?????.
    "detection": {
      "enabled": true,
      "updateEvent": "PY(ON_TOTAL_EFFICIENCY), ON_PANEL_MODE_CHANGED",
      "x": "{{py:sum({{pp.widthLeft}},233)}}",
      "y": 35,
      "alpha": "{{py:xvm.detection>0?100|60}}",
      "width": 70,
      "height": 25,
      "textFormat": { "color": "{{py:xvm.detection>0?0xFFC363|0xFFFFFF}}", "size": 15 },
      "format": "<font face='XVMSymbol' size='21' color='#F2F2F2'>&#x10A;</font> <b>{{py:xvm.detection}}</b>",
      "shadow": ${ "def.textFieldShadow" }
    },
    // During stun damage.
    // ???? ?? ?????????.
    "totalStun": {
      "enabled": true,
      "hotKeyCode": 56, "onHold": "true", "visibleOnHotKey": false,
      "updateEvent": "PY(ON_TOTAL_EFFICIENCY), ON_PANEL_MODE_CHANGED",
      "x": "{{py:sum({{pp.widthLeft}},303)}}",
      "y": 35,
      "alpha": "{{py:xvm.isStuns?{{py:xvm.numberStuns>0?100|60}}|0}}",
      "width": 70,
      "height": 25,
      "textFormat": { "color": "{{py:xvm.numberStuns>0?0xFFC363|0xFFFFFF}}", "size": 15 },
      "format": "<font face='XVMSymbol' size='21' color='#F2F2F2'>&#x10E;</font> <b>{{py:xvm.totalStun}}</b>",
      "shadow": ${ "def.textFieldShadow" }
    },
    // Stun count.
    // ?????????? ?????????.
    "totalStunCount": {
      "enabled": true,
      "hotKeyCode": 56, "onHold": "true", "visibleOnHotKey": true,
      "updateEvent": "PY(ON_TOTAL_EFFICIENCY), ON_PANEL_MODE_CHANGED",
      "x": "{{py:sum({{pp.widthLeft}},303)}}",
      "y": 35,
      "alpha": "{{py:xvm.isStuns?{{py:xvm.numberStuns>0?100|60}}|0}}",
      "width": 70,
      "height": 25,
      "textFormat": { "color": "{{py:xvm.numberStuns>0?0xFFC363|0xFFFFFF}}", "size": 15 },
      "format": "<font face='XVMSymbol' size='21' color='#F2F2F2'>&#x10E;</font> <b>{{py:xvm.numberStuns}}</b>",
      "shadow": ${ "def.textFieldShadow" }
    },
	"teamRatingLogo": {
      "enabled": false,
      "x": 230,
      "y": 1,
      "shadow": { "distance": 1, "angle": 90, "alpha": 80, "blur": 5, "strength": 1.5 },
      "textFormat": { "color": "0xF4EFE8", "size": 15 },
      "format": "<font face='xvm' size='20'>&#x103;</font>"
    },
    "teamRatingWinChance": {
      "enabled": false,
      "updateEvent": "PY(ON_UPDATE_TEAM_RATING)",
      "x": 250,
      "y": 2,
      "shadow": { "distance": 1, "angle": 90, "alpha": 80, "blur": 5, "strength": 1.5 },
      "textFormat": { "color": "0xF4EFE8", "size": 15 },
      "format": "<font color='{{py:alliesAliveRatingRatio>=90?#D042F3|{{py:alliesAliveRatingRatio>=75?#02C9B3|{{py:alliesAliveRatingRatio>=60?#60FF00|{{py:alliesAliveRatingRatio>=40?#F8F400|{{py:alliesAliveRatingRatio>=1?#FE7903|#FE0E00}}}}}}}}}}'>{{py:alliesAliveRatingRatio}}%</font>"
    },
    "teamRating": {
      "enabled": false,
      "updateEvent": "PY(ON_UPDATE_TEAM_RATING)",
      "x": 232,
      "y": 23,
      "textFormat": { "color": "0xF4EFE8", "size": 15 },
      "format": "Teams WN8: <font color='{{py:alliesAliveRatingRatio>=50?#00EE00|#EE0000}}'>{{py:alliesAliveRating}} {{py:alliesAliveRatingRatio=50?=|{{py:alliesAliveRatingRatio>50?>|<}}}} {{py:enemiesAliveRating}}</font>"
    },
    "TotalEfficiencyBg": {
      "enabled": false,    
      "x": 227, "y": -203, "screenVAlign": "bottom",
      "src": "xvm://res/icons/damageLog/bg.png"
    },
    "TotalEfficiency": {
        "enabled": false,
        "updateEvent": "PY(ON_TOTAL_EFFICIENCY)",       
        "x": 232, 
        "y": -195, 
        "width": 92, 
        "height": 85, 
        "screenVAlign": "bottom",
        "shadow": { "alpha": 45, "blur": 6, "strength": 2 },
        "textFormat": { "size": 15, "leading": -3 },
        "format": "<font face='mono'><img src='xvm://res/icons/damageLog/assist.png' vspace='-6'>{{py:xvm.totalAssist}}<br><img src='xvm://res/icons/damageLog/reflect.png' vspace='-6'>{{py:xvm.totalBlocked}}<font size='12'>-{{py:xvm.countBlockedHits}}x</font><br><img src='xvm://res/icons/damageLog/discover.png' vspace='-6'>{{py:xvm.detection}}</font>"   
    },
    "battleEfficiency": {
        "enabled": "{{pp.mode=0?{{battletype-key=epic_battle?false|true}}}}",
        "updateEvent": "PY(ON_BATTLE_EFFICIENCY)",
        "x": 119,
        "y": 37,
        "width": 215,
        "height": 25,
        "align": "center",
        "screenHAlign": "left",
        "screenVAlign": "top",
        "textFormat": { "font": "$TitleFont", "size": 16 },
        "shadow": { "distance": 1, "angle": 90, "color": "0x000000", "alpha": 90, "blur": 3, "strength": 2 },
        "format": "WN8: <font color='{{py:efficiencyWN8Color}}'>{{py:efficiencyWN8}}</font>"
    }
  }
}