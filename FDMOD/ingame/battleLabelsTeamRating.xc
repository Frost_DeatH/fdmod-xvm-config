﻿/**
 *
 * LastEdit:06.07.2018
 *
 */
{
  "def": {
	"teamWinChance": {
      "enabled": true,
      "updateEvent": "PY(ON_UPDATE_TEAM_RATING)",
      "x": 20,
      "y": 440,
      "align": "left",
      "screenHAlign": "left",
      "shadow": { "distance": 0, "angle": 45, "alpha": 90, "blur": 4, "strength": 2 },
      "textFormat": { "font": "Calibri", "color": "0xEEEEEE", "size": 16, "align": "left" },
      "format": "<font face='xvm' size='20'>&#x103;</font><b>  <font color='{{py:alliesAliveRatingRatio>=90?#D042F3|{{py:alliesAliveRatingRatio>=75?#02C9B3|{{py:alliesAliveRatingRatio>=65?#60FF00|{{py:alliesAliveRatingRatio>=45?#F8F400|{{py:alliesAliveRatingRatio>=25?#FE7903|#FE0E00}}}}}}}}}}'>{{py:alliesAliveRatingRatio}}%</font></b>"
	},
	"altMessage": {
      "enabled": true,
      "updateEvent": "PY(ON_UPDATE_TEAM_RATING)",
      "hotKeyCode": 23,
      "onHold": "false",
      "visibleOnHotKey": false,
      "x": 20,
      "y": 460,
      "align": "left",
      "screenHAlign": "left",
      "shadow": { "distance": 0, "angle": 45, "alpha": 90, "blur": 4, "strength": 2 },
      "textFormat": { "font": "Calibri", "color": "0xEEEEEE", "size": 12, "align": "left" },
      "format": "<i>(I = more info)</i>"
	},
	"teamRating": {
      "enabled": true,
      "updateEvent": "PY(ON_UPDATE_TEAM_RATING)",
      "hotKeyCode": 23,
      "onHold": "false",
      "visibleOnHotKey": true,
      "x": 20,
      "y": 460,
      "align": "left",
      "screenHAlign": "left",
      "shadow": { "distance": 0, "angle": 45, "alpha": 90, "blur": 4, "strength": 2 },
      "textFormat": { "font": "Calibri", "color": "0xEEEEEE", "size": 15, "align": "left" },
      "format": "<b><font color='#EEEEEE'>Tím WN8:</font>  <font color='{{py:c_alliesAliveRatingRatio}}'>{{py:alliesAliveRating}} {{py:alliesAliveRatingRatio=0?=|{{py:alliesAliveRatingRatio>0?>|<}}}} {{py:enemiesAliveRating}}</font></b>"
      //"format": "<b>TeamWN8:  <font color='{{py:alliesAliveRatingRatio>=90?#D042F3|{{py:alliesAliveRatingRatio>=75?#02C9B3|{{py:alliesAliveRatingRatio>=65?#60FF00|{{py:alliesAliveRatingRatio>=45?#F8F400|{{py:alliesAliveRatingRatio>=25?#FE7903|#FE0E00}}}}}}}}}}'>{{py:alliesAliveRating}} {{py:alliesAliveRatingRatio=0?=|{{py:alliesAliveRatingRatio>0?>|<}}}} {{py:enemiesAliveRating}}</font></b>"
	},
	"teamHP": {
      "enabled": false,
      "updateEvent": "PY(ON_UPDATE_HP)",
      "hotKeyCode": 56,
      "onHold": "false",
      "visibleOnHotKey": true,
      "x": 20,
      "y": 470,
      "screenHAlign": "left",
      "align": "left",
      "shadow": { "distance": 0, "angle": 45, "alpha": 90, "blur": 4, "strength": 2 },
      "textFormat": { "font": "Calibri", "size": 15, "align": "left" },
      "format": "<b><font color='#EEEEEE'>TeamHP:</font>{{py:xvm.total_hp.text}}</b>"
	}
	//"placeholder": {
      //"enabled": false,
      //"updateEvent": "",
      //"hotKeyCode": 56,
      //"onHold": "false",
      //"visibleOnHotKey": true,
      //"x": 20,
      //"y": 485,
      //"screenHAlign": "left",
      //"align": "left",
      //"shadow": { "distance": 0, "angle": 45, "alpha": 90, "blur": 4, "strength": 2 },
      //"textFormat": { "font": "Calibri", "size": 15, "align": "left" },
      //"format": ""
	//}
  }
}