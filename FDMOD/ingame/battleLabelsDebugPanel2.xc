﻿{
  "def": {
    //fps + ping + lag indicator
	"debugPanel": {
      "enabled": true,
	  "updateEvent": "ON_EVERY_FRAME",
      "x": 10,
      "y": 0,
      "screenHAlign": "left",
	  "screenVAlign": "top",
      "shadow": { "enabled": true, "color": "#000000", "distance": 0, "angle": 90, "alpha": 100, "blur": 4, "strength": 2 },
      "textFormat": { "font": "$TitleFont", "color": "0xCCCCCC", "size": 13, "align": "left" },
      "format": "<textformat tabstops='[75,125]'>FPS  {{py:xvm.fps_replay}}{{py:xvm.fps%3d}}<tab>PING  {{py:xvm.ping%3d}}<tab>{{py:xvm.lag?<img src='img://gui/maps/icons/library/alertBigIcon.png' width='22' height='22' align='baseline' vspace='-7'>|<img src='img://gui/maps/icons/library/complete.png' width='24' height='24' align='baseline' vspace='-7'>}}</textformat>"
	},
	//ping indicator bar
	"debugPanelbg": {
	  "enabled": true,
	  "updateEvent": "ON_EVERY_FRAME",
	  "layer": "bottom",
      "x": 0,
      "y": 20,
	  "height": 28,
	  "width": 150,
      "alpha": 100,
	  "screenHAlign": "left",
	  "screenVAlign": "top",
      "format": "{{py:xvm.ping>999?<img src='cfg://FDMOD/icons/pingStatus/0.png'>|{{py:xvm.ping>200?<img src='cfg://FDMOD/icons/pingStatus/1.png'>|{{py:xvm.ping>150?<img src='cfg://FDMOD/icons/pingStatus/2.png'>|{{py:xvm.ping>100?<img src='cfg://FDMOD/icons/pingStatus/3.png'>|{{py:xvm.ping>70?<img src='cfg://FDMOD/icons/pingStatus/4.png'>|{{py:xvm.ping>50?<img src='cfg://FDMOD/icons/pingStatus/5.png'>|{{py:xvm.ping>40?<img src='cfg://FDMOD/icons/pingStatus/6.png'>|{{py:xvm.ping>30?<img src='cfg://FDMOD/icons/pingStatus/7.png'>|{{py:xvm.ping>20?<img src='cfg://FDMOD/icons/pingStatus/8.png'>|{{py:xvm.ping>10?<img src='cfg://FDMOD/icons/pingStatus/9.png'>|<img src='cfg://FDMOD/icons/pingStatus/10.png'>}}}}}}}}}}}}}}}}}}}}"
    }
  }
}