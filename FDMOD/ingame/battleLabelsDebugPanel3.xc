﻿{
  "def": {
	"debugPanelClock": {
      "enabled": true,
      "updateEvent": "ON_EVERY_SECOND",
      "x": 170,
      "y": 5,
	  //"borderColor": "0x000000", //used for debugging
      "width": 55,               //used for debugging
      "height": 25,              //used for debugging
	  "alpha": 100,
      "screenHAlign": "left",
	  "screenVAlign": "top",
      "shadow": { "enabled": true, "color": "#000000", "distance": 1, "angle": 85, "alpha": 90, "blur": 2, "strength": 1 },
      "textFormat": { "font": "Cuprum", "color": "0xF4EFE8", "size": 16, "align": "left" },
      "format": "{{py:xvm.formatDate('%H:%M:%S')}}"
},
    "debugPanel": {
      "enabled": true,
	  "updateEvent": "ON_EVERY_FRAME",
      "x": 4,
      "y": 2,
	  //"borderColor": "0x000000",
      "screenHAlign": "left",
	  "screenVAlign": "top",
      "shadow": { "enabled": true, "color": "#000000", "distance": 1, "angle": 85, "alpha": 65, "blur": 2, "strength": 2 },
      "textFormat": { "font": "Cuprum", "color": "0xEEEEEE", "size": 16, "align": "left" },
      "format": "<textformat tabstops='[65,132]'>FPS: {{py:xvm.fps%3d}}{{py:xvm.fps_replay?({{py:xvm.fps_replay}})}}<tab>PING: {{py:xvm.ping%3d}}<tab>{{py:xvm.lag?<img src='img://gui/maps/icons/library/alertBigIcon.png' width='24' height='24' vspace='-7'>|<img src='img://gui/maps/icons/library/complete.png' width='24' height='24' vspace='-7'>}}</textformat>"
	},
	"debugPanelpingBar": {
	  "enabled": true,
	  "updateEvent": "ON_EVERY_FRAME",
	  "layer": "bottom",
      "x": 0,
      "y": 18,
	  //"borderColor": "0x000000",
	  "height": 25,
	  "width": 225,
      "alpha": 100,
	  "screenHAlign": "left",
	  "screenVAlign": "top",
      "format": "{{py:xvm.ping>999?<img src='cfg://FDMOD/icons/pingBar/0.png'>|{{py:xvm.ping>200?<img src='cfg://FDMOD/icons/pingBar/1.png'>|{{py:xvm.ping>150?<img src='cfg://FDMOD/icons/pingBar/2.png'>|{{py:xvm.ping>100?<img src='cfg://FDMOD/icons/pingBar/3.png'>|{{py:xvm.ping>70?<img src='cfg://FDMOD/icons/pingBar/4.png'>|{{py:xvm.ping>50?<img src='cfg://FDMOD/icons/pingBar/5.png'>|{{py:xvm.ping>40?<img src='cfg://FDMOD/icons/pingBar/6.png'>|{{py:xvm.ping>30?<img src='cfg://FDMOD/icons/pingBar/7.png'>|{{py:xvm.ping>20?<img src='cfg://FDMOD/icons/pingBar/8.png'>|{{py:xvm.ping>10?<img src='cfg://FDMOD/icons/pingBar/9.png'>|<img src='cfg://FDMOD/icons/pingBar/10.png'>}}}}}}}}}}}}}}}}}}}}"
    }
  }
}