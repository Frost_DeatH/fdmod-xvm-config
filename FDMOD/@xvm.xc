﻿/**
 * Hlavny konfig pre FDMOD CZ balicek.
 *
 */
{
  "configVersion": "1.4.0.2a",
  "autoReloadConfig": true,
  "language": ${"includes/fdmod_lang.xc":"."},
  "region": "auto",
  "definition": {
    
    "author": "Frost_DeatH",

    "description": "Default settings for XVM and FDMOD",

    "url": "http://www.frost-zone.eu/fdmod",

    "date": "16.03.2019",

    "gameVersion": "1.4.0.2",

    "modMinVersion": "2.77"
  },
  "module-off": {
    "damageLog": true,                  // false - 
    "fragCorrelation": true,            // false - 
    "hitLog": true,                     // false - 
    "lastHit": true                   // false - 
  },
// HANGAR polozky xvm-ka =======================================
  "custom_texts":      ${"hangar/@custom_texts.xc":"custom_texts"},
  "login":             ${"hangar/login.xc":"login"},
  "hangar":            ${"hangar/hangar.xc":"hangar"},
  "userInfo":          ${"hangar/userInfo.xc":"userInfo"},
  "battleResults":     ${"hangar/battleResults.xc":"battleResults"},
  
// INGAME polozky xvm-ka =======================================
  
  "battle":            ${"ingame/battle.xc":"battle"},
  "boosters":          ${"hangar/boosters.xc":"boosters"},
  "expertPanel":       ${"ingame/battle.xc":"expertPanel"},  
  "fragCorrelation":   ${"ingame/battle.xc":"fragCorrelation"},   
  "battleLoading":     ${"ingame/battleLoading.xc":"battleLoading"},
  "battleLoadingTips": ${"ingame/battleLoadingTips.xc":"battleLoadingTips"},  
  "statisticForm":     ${"ingame/statisticForm.xc":"statisticForm"},
  "playersPanel":      ${"ingame/playersPanel.xc":"playersPanel"},
  "hitLog":            ${"ingame/hitLog.xc":"hitLog"},
  "captureBar":        ${"ingame/captureBar.xc":"captureBar"},
  "squad":             ${"ingame/squad.xc":"squad"},
  "hotkeys":           ${"ingame/hotkeys.xc":"hotkeys"},
  "battleLabels":      ${"ingame/battleLabels.xc":"labels"},
  "damageLog":         ${"ingame/damageLog.xc":"damageLog"},
  "safeShot":          ${"ingame/safeShot.xc":"safeShot"},

// MINIMAP polozky xvm-ka ======================================
  
  "minimap":           ${"minimap/minimap.xc":"minimap"},
  "minimapAlt":        ${"minimap/minimapAlt.xc":"minimap"},
  
// MARKERS polozky xvm-ka ======================================
 
  "markers":           ${"markers/markers.xc":"markers"},

// OTHER - ine polozky xvm-ka ==================================
  
  "colors":            ${"other/colors.xc":"colors"},
  "alpha":             ${"other/alpha.xc":"alpha"},
  "texts":             ${"other/texts.xc":"texts"},
  "iconset":           ${"other/iconset.xc":"iconset"},
  "vehicleNames":      ${"other/vehicleNames.xc":"vehicleNames"},
  "export":            ${"other/export.xc":"export"},
  "tooltips":          ${"other/tooltips.xc":"tooltips"},
  "sounds":            ${"other/sounds.xc":"sounds"},
  "xmqp":              ${"other/xmqp.xc":"xmqp"},
  "tweaks":            ${"other/tweaks.xc":"tweaks"}

// OPTIONS FDMOD - rychle nastavenie xvm-ka ====================
        
}