﻿/**
 * Options for dynamic transparency. Values ??from smallest to largest.
 * ????????? ???????????? ????????????. ???????? ?? ???????? ? ????????.
 */
{
  // Transparency values for substitutions.
  // ???????? ???????????? ??? ???????????.
  "def": {
    // Dynamic transparency by various statistical parameters.
    // ???????????? ???????????? ?? ????????? ?????????????? ???????????.
    "alphaRating": {
      "very_bad":      "100",  // very bad   / ????? ?????
      "bad":           "70",   // bad        / ?????
      "normal":        "40",   // normal     / ??????
      "good":          "10",   // good       / ??????
      "very_good":     "0",    // very good  / ????? ??????
      "unique":        "0"     // unique     / ?????????
    },
    // Dynamic transparency by remaining health points.
    // ???????????? ???????????? ?? ??????????? ?????? ?????????.
    "alphaHP": {
      "very_low":      "100",  // very low       / ????? ??????
      "low":           "75",   // low            / ??????
      "average":       "50",   // average        / ???????
      "above_average": "0"     // above-average  / ???? ????????
    }
  },
  "alpha": {
    // Dynamic transparency by spotted status
    // ???????????? ???????????? ?? ??????? ???????
    "spotted": {
      "neverSeen":      100,
      "lost":           100,
      "spotted":        100,
      "dead":           100,
      "neverSeen_arty": 100,
      "lost_arty":      100,
      "spotted_arty":   100,
      "dead_arty":      100
    },
    // Dynamic transparency by remaining health.
    // ???????????? ???????????? ?? ??????????? ????????.
    "hp": [
      { "value": 200,  "alpha": ${"def.alphaHP.very_low"     } }, // alpha for HP <= 200     
      { "value": 400,  "alpha": ${"def.alphaHP.low"          } }, // alpha for HP <= 400     
      { "value": 1000, "alpha": ${"def.alphaHP.average"      } }, // alpha for HP <= 1000    
      { "value": 9999, "alpha": ${"def.alphaHP.above_average"} }  // alpha for HP > 1000     
    ],                                                                                          
    // Dynamic transparency by percentage of remaining health.                                  
    // ???????????? ???????????? ?? ???????? ??????????? ????????.                              
    "hp_ratio": [                                                                               
      { "value": 10.4, "alpha": ${"def.alphaHP.very_low"     } }, // alpha for HP <= 10%     
      { "value": 25.4, "alpha": ${"def.alphaHP.low"          } }, // alpha for HP <= 25%     
      { "value": 50.4, "alpha": ${"def.alphaHP.average"      } }, // alpha for HP <= 50%     
      { "value": 100,  "alpha": ${"def.alphaHP.above_average"} }  // alpha for HP > 50%      
    ],
    // Dynamic transparency for XVM Scale
    // ???????????? ???????????? ?? ????? XVM
    "x": [
      { "value": 16.4, "alpha": ${"def.alphaRating.very_bad" } }, // 00 - 16 - very bad  (20% of players)              
      { "value": 33.4, "alpha": ${"def.alphaRating.bad"      } }, // 17 - 33 - bad       (better than 20% of players)  
      { "value": 52.4, "alpha": ${"def.alphaRating.normal"   } }, // 34 - 52 - normal    (better than 60% of players)  
      { "value": 75.4, "alpha": ${"def.alphaRating.good"     } }, // 53 - 75 - good      (better than 90% of players)  
      { "value": 92.4, "alpha": ${"def.alphaRating.very_good"} }, // 76 - 92 - very good (better than 99% of players)  
      { "value": 999,  "alpha": ${"def.alphaRating.unique"   } }  // 93 - XX - unique    (better than 99.9% of players)
    ],
    // Dynamic transparency by efficiency
    // ???????????? ???????????? ?? ?????????????
    "eff": [
      { "value": 598,  "alpha": ${"def.alphaRating.very_bad" } }, //    0 - 598  - very bad  (20% of players)
      { "value": 874,  "alpha": ${"def.alphaRating.bad"      } }, //  599 - 874  - bad       (better than 20% of players)
      { "value": 1079, "alpha": ${"def.alphaRating.normal"   } }, //  875 - 1079 - normal    (better than 60% of players)
      { "value": 1540, "alpha": ${"def.alphaRating.good"     } }, // 1080 - 1540 - good      (better than 90% of players)
      { "value": 1868, "alpha": ${"def.alphaRating.very_good"} }, // 1541 - 1868 - very good (better than 99% of players)
      { "value": 9999, "alpha": ${"def.alphaRating.unique"   } }  // 1869 - *    - unique    (better than 99.9% of players)
    ],
    // Dynamic transparency by WTR rating
    // ???????????? ???????????? ?? ???????? WTR
    "wtr": [
      { "value": 2631,  "alpha": ${"def.alphaRating.very_bad" } }, //    0 - 2631 - very bad  (20% of players)
      { "value": 4464,  "alpha": ${"def.alphaRating.bad"      } }, // 2632 - 4464 - bad       (better than 20% of players)
      { "value": 6249,  "alpha": ${"def.alphaRating.normal"   } }, // 4465 - 6249 - normal    (better than 60% of players)
      { "value": 8141,  "alpha": ${"def.alphaRating.good"     } }, // 6250 - 8141 - good      (better than 90% of players)
      { "value": 9460,  "alpha": ${"def.alphaRating.very_good"} }, // 8142 - 9460 - very good (better than 99% of players)
      { "value": 99999, "alpha": ${"def.alphaRating.unique"   } }  // 9461 - *    - unique    (better than 99.9% of players)
    ],
    // Dynamic transparency by WN8 rating
    // ???????????? ???????????? ?? ???????? WN8
    "wn8": [
      { "value": 397,  "alpha": ${"def.alphaRating.very_bad" } }, //    0 - 397  - very bad  (20% of players)
      { "value": 914,  "alpha": ${"def.alphaRating.bad"      } }, //  398 - 914  - bad       (better than 20% of players)
      { "value": 1450, "alpha": ${"def.alphaRating.normal"   } }, //  915 - 1489 - normal    (better than 60% of players)
      { "value": 2100, "alpha": ${"def.alphaRating.good"     } }, // 1490 - 2231 - good      (better than 90% of players)
      { "value": 2700, "alpha": ${"def.alphaRating.very_good"} }, // 2232 - 2979 - very good (better than 99% of players)
      { "value": 9999, "alpha": ${"def.alphaRating.unique"   } }  // 2980 - *    - unique    (better than 99.9% of players)
    ],
    // Dynamic transparency by WG rating
    // ???????????? ???????????? ?? ???????? WG
    "wgr": [
      { "value": 2578,  "alpha": ${"def.alphaRating.very_bad" } }, //     0 - 2578  - very bad  (20% of players)
      { "value": 4521,  "alpha": ${"def.alphaRating.bad"      } }, //  2579 - 4521  - bad       (better than 20% of players)
      { "value": 6630,  "alpha": ${"def.alphaRating.normal"   } }, //  4522 - 6630  - normal    (better than 60% of players)
      { "value": 8884,  "alpha": ${"def.alphaRating.good"     } }, //  6631 - 8884  - good      (better than 90% of players)
      { "value": 10347, "alpha": ${"def.alphaRating.very_good"} }, //  8885 - 10347 - very good (better than 99% of players)
      { "value": 99999, "alpha": ${"def.alphaRating.unique"   } }  // 10348 - *     - unique    (better than 99.9% of players)
    ],
    // Dynamic transparency by win percent
    // ???????????? ???????????? ?? ???????? ?????
    "winrate": [
      { "value": 46.49, "alpha": ${"def.alphaRating.very_bad" } },
      { "value": 48.49, "alpha": ${"def.alphaRating.bad"      } },
      { "value": 52.49, "alpha": ${"def.alphaRating.normal"   } },
      { "value": 57.49, "alpha": ${"def.alphaRating.good"     } },
      { "value": 64.49, "alpha": ${"def.alphaRating.very_good"} },
      { "value": 100,   "alpha": ${"def.alphaRating.unique"   } }
    ],
    // Dynamic transparency by kilo-battles
    // ???????????? ???????????? ?? ?????????? ????-????
    "kb": [
      { "value": 2,   "alpha": ${"def.alphaRating.very_bad" } },
      { "value": 6,   "alpha": ${"def.alphaRating.bad"      } },
      { "value": 16,  "alpha": ${"def.alphaRating.normal"   } },
      { "value": 30,  "alpha": ${"def.alphaRating.good"     } },
      { "value": 43,  "alpha": ${"def.alphaRating.very_good"} },
      { "value": 999, "alpha": ${"def.alphaRating.unique"   } }
    ],
    // Dynamic transparency by average level of player tanks
    // ???????????? ???????????? ?? ???????? ?????? ?????? ??????
    "avglvl": [
      { "value": 1,   "alpha": ${"def.alphaRating.very_bad" } },
      { "value": 2,   "alpha": ${"def.alphaRating.bad"      } },
      { "value": 4,   "alpha": ${"def.alphaRating.normal"   } },
      { "value": 6,   "alpha": ${"def.alphaRating.good"     } },
      { "value": 8,   "alpha": ${"def.alphaRating.very_good"} },
      { "value": 10,  "alpha": ${"def.alphaRating.unique"   } }
    ],
    // Dynamic transparency by battles on current tank
    // ???????????? ???????????? ?? ?????????? ???? ?? ??????? ?????
    "t_battles": [
      { "value": 99,    "alpha": ${"def.alphaRating.very_bad" } },
      { "value": 249,   "alpha": ${"def.alphaRating.bad"      } },
      { "value": 499,   "alpha": ${"def.alphaRating.normal"   } },
      { "value": 999,   "alpha": ${"def.alphaRating.good"     } },
      { "value": 1799,  "alpha": ${"def.alphaRating.very_good"} },
      { "value": 99999, "alpha": ${"def.alphaRating.unique"   } }
    ],
    // Dynamic transparency by average damage on current tank
    // ???????????? ???????????? ?? ???????? ????? ?? ??? ?? ??????? ?????
    "tdb": [
      { "value": 499,  "alpha": ${"def.alphaRating.very_bad" } },
      { "value": 749,  "alpha": ${"def.alphaRating.bad"      } },
      { "value": 999,  "alpha": ${"def.alphaRating.normal"   } },
      { "value": 1799, "alpha": ${"def.alphaRating.good"     } },
      { "value": 2499, "alpha": ${"def.alphaRating.very_good"} },
      { "value": 9999, "alpha": ${"def.alphaRating.unique"   } }
    ],
    // Dynamic transparency by average damage efficiency on current tank
    // ???????????? ???????????? ?? ????????????? ????? ?? ??? ?? ??????? ?????
    "tdv": [
      { "value": 0.5, "alpha": ${"def.alphaRating.very_bad" } },
      { "value": 0.7, "alpha": ${"def.alphaRating.bad"      } },
      { "value": 0.9, "alpha": ${"def.alphaRating.normal"   } },
      { "value": 1.2, "alpha": ${"def.alphaRating.good"     } },
      { "value": 1.9, "alpha": ${"def.alphaRating.very_good"} },
      { "value": 15,  "alpha": ${"def.alphaRating.unique"   } }
    ],
    // Dynamic transparency by average frags per battle on current tank
    // ???????????? ???????????? ?? ???????? ?????????? ?????? ?? ??? ?? ??????? ?????
    "tfb": [
      { "value": 0.5, "alpha": ${"def.alphaRating.very_bad" } },
      { "value": 0.7, "alpha": ${"def.alphaRating.bad"      } },
      { "value": 0.9, "alpha": ${"def.alphaRating.normal"   } },
      { "value": 1.2, "alpha": ${"def.alphaRating.good"     } },
      { "value": 1.9, "alpha": ${"def.alphaRating.very_good"} },
      { "value": 15,  "alpha": ${"def.alphaRating.unique"   } }
    ],
    // Dynamic transparency by average number of spotted enemies per battle on current tank
    // ???????????? ???????????? ?? ???????? ?????????? ??????????? ?????? ?? ??? ?? ??????? ?????
    "tsb": [
      { "value": 0.5, "alpha": ${"def.alphaRating.very_bad" } },
      { "value": 0.7, "alpha": ${"def.alphaRating.bad"      } },
      { "value": 0.9, "alpha": ${"def.alphaRating.normal"   } },
      { "value": 1.2, "alpha": ${"def.alphaRating.good"     } },
      { "value": 1.9, "alpha": ${"def.alphaRating.very_good"} },
      { "value": 15,  "alpha": ${"def.alphaRating.unique"   } }
    ]
  }
}
