﻿{
  "carousel": {
    "enabled": ${"../fdmod_carousel_enable.xc":"."},
    "cellType": "default",
    "normal": ${"carouselTemplates.xc":"normal"},
    "small": ${"carouselTemplates.xc":"small"},
    "rows": ${"../fdmod_carousel_Rows.xc":"."},
    "backgroundAlpha": 95,
    "scrollingSpeed": 1,
    "hideBuyTank": false,
    "hideBuySlot": false,
    "showTotalSlots": false,    
    "showUsedSlots": true,
    "enableLockBackground": true,    
    "filters": {
      "params": { "enabled": true },
      "bonus": { "enabled": true },
      "favorite": { "enabled": true }
    },
    "filtersPadding": {
      "horizontal": 11,
      "vertical": 13
    },
    "nations_order": [],
    "types_order": ["lightTank", "mediumTank", "heavyTank", "AT-SPG", "SPG"],
    "sorting_criteria": ["nation", "type", "level"],
    "suppressCarouselTooltips": false
  }
}