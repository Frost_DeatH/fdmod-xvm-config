﻿{
  "custom_texts": {
    "carousel": {
      "nation": {
        "ussr": "ussr_star",
        "germany": "germany_cross",
        "usa": "usa_star",
        "france": "french_rose",
        "uk": "britain_color",
        "china": "china_star",
        "japan": "japanese_sun",
        "czech": "czech_round",
        "swedish": "swedish_flag"
      },
      "type": "{{v.type={{l10n:LT}}?lightTank}}{{v.type={{l10n:MT}}?mediumTank}}{{v.type={{l10n:HT}}?heavyTank}}{{v.type={{l10n:SPG}}?SPG}}{{v.type={{l10n:TD}}?AT-SPG}}"
    },
    "hitLog": {
      "vtype": {
        "LT": "<font face='NDO'>&#x0041;</font>",
        "MT": "<font face='NDO'>&#x0042;</font>",
        "HT": "<font face='NDO'>&#x0043;</font>",
        "SPG": "<font face='NDO'>&#x0045;</font>",
        "TD": "<font face='NDO'>&#x0044;</font>"
      }
    }      
  }
}