﻿/**
 * Widgets templates.
 * ??????? ????????.
 */
{
  "clock": {
    // Show clock in hangar.
    // ?????????? ???? ? ??????.
    "enabled": true,
    // layer - "bottom", "normal" (default), "top".
    // ???? - "bottom", "normal" (??-?????????), "top".
    "layer": "top",
    "type": "extrafield",
    "formats": [
      { // ??????? ????????
        "x": 4,
        "y": 51,
        "screenHAlign": "right",
        "format": "<img src='xvm://res/icons/clock/clockBg.png'>"
      },
      {
        "updateEvent": "ON_EVERY_SECOND",
        // Horizontal position.
        // ????????? ?? ???????????.
        "x": -10,
        // Vertical position.
        // ????????? ?? ?????????.
        "y": 38,
        // Width.
        // ??????.
        "width": 200,
        // Height.
        // ??????.
        "height": 50,
        // Horizontal alignment of field at screen ("left", "center", "right").
        // ?????????????? ???????????? ???? ?? ?????? ("left", "center", "right").
        "screenHAlign": "right",
        "shadow": {
          // false - no shadow
          // false - ??? ????
          "enabled": true,
          "distance": 0,             // (in pixels)     / offset distance / ????????? ????????
          "angle": 0,                // (0.0 .. 360.0)  / offset angle    / ???? ????????
          "color": "0x000000",       // "0xXXXXXX"      / color           / ????
          "alpha": 70,               // (0 .. 100)      / opacity         / ????????????
          "blur": 4,                 // (0.0 .. 255.0)  / blur            / ????????
          "strength": 2              // (0.0 .. 255.0)  / intensity       / ?????????????
        },
        "textFormat": { "align": "right", "valign": "bottom", "color": "0xFFAA33" },
        "format": "<font face='$FieldFont'><textformat leading='-38'><font size='36'>{{py:xvm.formatDate('%H:%M')}}</font><br></textformat><textformat rightMargin='85' leading='-2'>{{py:xvm.formatDate('%A')}}<br><font size='15'>{{py:xvm.formatDate('%d %b %Y')}}</font></textformat></font>"
      }
    ]
  },
      "boosters":{
        "enabled": true, "layer": "top", "type": "extrafield",
        "formats": [
          {
            "updateEvent": "ON_EVERY_SECOND",
            "x": 74, "y": 51, "width": 330, "height": 40, "alpha": "{{py:bst.type(0)?100|{{py:bst.typeCR(0)?100|0}}}}",
            "format": "<img src='cfg://FDMOD/img/lobby/boosters/background.png' width='320' height='34'>"
          },
          {
            "updateEvent": "ON_EVERY_SECOND",
            "x": 85, "y": 28, "width": 330, "height": 60,
            "format": "<img src='cfg://FDMOD/img/lobby/boosters/{{py:bst.type(1)}}.png' width='64' height='54'><img src='cfg://FDMOD/img/lobby/boosters/{{py:bst.type(2)}}.png' width='64' height='54'><img src='cfg://FDMOD/img/lobby/boosters/{{py:bst.type(3)}}.png' width='64' height='54'><img src='cfg://FDMOD/img/lobby/boosters/CR/{{py:bst.typeCR(1)}}.png' width='64' height='54'><img src='cfg://FDMOD/img/lobby/boosters/CR/{{py:bst.typeCR(2)}}.png' width='64' height='54'>"
          },
          {
            "updateEvent": "ON_EVERY_SECOND",
            "x": 120, "y": 69, "width": 330, "height": 40,
            "shadow": { "alpha": 70, "blur": 3, "color": "0x0000CD", "strength": 1 },
            "format": "<textformat tabstops='[65,130,195,260]'><font color='#CCFFFF' face='$FieldFont' size='12'>{{py:bst.type(1)?{{py:bst.leftTime(1)}}<tab>}}{{py:bst.type(2)?{{py:bst.leftTime(2)}}<tab>}}{{py:bst.type(3)?{{py:bst.leftTime(3)}}<tab>}}{{py:bst.typeCR(1)?{{py:bst.leftTimeCR(1)}}<tab>}}{{py:bst.leftTimeCR(2)}}</font></textformat>"
          }
        ]
      },
      "clanReserv": {
        "enabled": false,
        "layer": "top",
        "type": "extrafield",
        "formats": [
          { "updateEvent": "ON_EVERY_SECOND",
            "x": 1300,
            "y": 0,
            "width": 400,
            "height": 80,
            "textFormat": { "color": "0xA8A888", "size": 12},
            "format": "<textformat tabstops='[50]'>{{py:bst.leftTimeCR(1)}}<tab>{{py:bst.nameCR(1)}}</textformat>\n<textformat tabstops='[50]'>{{py:bst.leftTimeCR(2)}}<tab>{{py:bst.nameCR(2)}}</textformat>"
          }
        ]
      },
  "statistics": {
    "enabled": false,
    "layer": "normal",
    "type": "extrafield",
    "formats": [
      { // background image, left part
        // ??????? ????????, ????? ?????.
        "x": 66,
        "y": 193,
        "screenHAlign": "center",
        "format": "<img src='xvm://res/icons/clock/clockBg.png' width='160' height='80'>"
      },
      { // background image, right part
        // ??????? ????????, ?????? ?????.
        "x": 390,
        "y": 193,
        "screenHAlign": "center",
        "scaleX": -1,
        "format": "<img src='xvm://res/icons/clock/clockBg.png' width='160' height='80'>"
      },
      { // text block
        // ????????? ????
        "updateEvent": "ON_MY_STAT_LOADED",
        "x": 185,
        "y": 198,
        "width": 320,
        "height": 150,
        "screenHAlign": "center",
        "shadow": { "alpha": 80, "blur": 4, "strength": 2 },
        "textFormat": { "color": "0x959688", "size": 15 },
        "format": "<font size='13'>{{l10n:General stats}} (<font color='#F9F1BC'>{{py:xvm.formatDate('%Y-%m-%d')}}</font>)</font>\n{{l10n:WN8}}: <font color='{{mystat.c_xwn8}}'>{{mystat.xwn8}} ({{mystat.wn8}})</font> {{l10n:EFF}}: <font color='{{mystat.c_eff}}'>{{mystat.xeff}} ({{mystat.eff}})</font>\n{{l10n:Avg level}}: <font color='{{mystat.c_avglvl}}'>{{mystat.avglvl%.2f}}</font>\n<font size='13'>{{l10n:Wins}}: <font color='{{mystat.c_winrate}}'>{{mystat.winrate%.2f~%}}</font>  ({{py:winrate_next(0.5)}} / {{py:winrate_next(1)}})</font>"
      }
    ]
  }
}
